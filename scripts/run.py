from os import system, listdir
from os.path import join
from sys import argv, stdout, stderr


# sanitize it
argv = [arg.lower() for arg in argv[1:]]

# check for empty
if len(argv) == 0:
    print("no arguments passed", file=stderr)
    exit(1)

# check for all
if len(argv) == 1 and argv[0] == "all":
    argv = listdir("src")

# iter over args
run_path = join("bin", "{0}.exe")
for arg in argv:
    # run it
    print(f"running {arg}:")
    ret = system(run_path.format(arg))
    if ret != 0:
        exit(ret)
