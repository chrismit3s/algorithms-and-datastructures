from os import system, listdir
from os.path import join
from sys import argv, stdout, stderr


# sanitize it
argv = [arg.lower() for arg in argv[1:]]

# check for empty
if len(argv) == 0:
    print("no arguments passed", file=stderr)
    exit(1)

# check for all
if len(argv) == 1 and argv[0] == "all":
    argv = listdir("src")

# iter over args
out_path = join("bin", "{0}.exe")
in_path = join("src", "{0}", "{0}-c", "main.c")
for arg in argv:
    # compile it
    print(f"compiling {arg}", end="", flush=True)
    ret = system(f"gcc -o {out_path.format(arg)} {in_path.format(arg)}")
    if ret != 0:
        print(f"cannot find {arg}", file=stderr)
        exit(ret)
    print(" - done")
