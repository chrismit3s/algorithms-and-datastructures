import greenfoot.*;
import java.util.*;
import java.lang.reflect.*;

public class MyWorld extends World {
    public static final int BUTTON_X = 100, BUTTON_Y = 30, BUTTON_SPACE = 10;
    public static final int NODE_TEXT_SIZE = 20, NODE_BORDER = 3;
    public static final int LEVEL_STEP = 40;
    public static final int WEIGHT_RADIUS = 12;
    public static final Color NODE_COLOR = Color.BLUE, FIND_COLOR = Color.MAGENTA, SELECT_COLOR = Color.GREEN;
    public static final Class<?>[] BUTTONS = {
            FindButton.class,
        };
    private Random rng;
    private Graph<Node> graph;
    private Node waiting;
    private long prevClickTime;

    public static void writeCentered(GreenfootImage img, String text, int size, Color color, int x, int y) {
        GreenfootImage textImg = new GreenfootImage(text, size, color, null);  
        x -= textImg.getWidth() / 2;
        y -= textImg.getHeight() / 2;
        img.drawImage(textImg, x, y);
    }

    public static void writeCentered(GreenfootImage img, String text, int size, Color color) {
        writeCentered(img, text, size, color, img.getWidth() / 2, img.getHeight() / 2);
    }

    public MyWorld() throws InstantiationException,
    IllegalAccessException,
    IllegalArgumentException,
    InvocationTargetException {
        super(600, 400, 1);

        this.rng = new Random();
        this.graph = new Graph();
        this.waiting = null;
        this.prevClickTime = System.currentTimeMillis();

        for (int i = 0; i != MyWorld.BUTTONS.length; ++i)
            this.addObject((Button)MyWorld.BUTTONS[i].getDeclaredConstructors()[0].newInstance(),
                MyWorld.BUTTON_SPACE + MyWorld.BUTTON_X / 2 + (MyWorld.BUTTON_SPACE + MyWorld.BUTTON_X) * i,
                MyWorld.BUTTON_SPACE + MyWorld.BUTTON_Y / 2);
    }

    public void addToEdgeQueue(Node node) {
        // not enough time passed (to filter bounce)
        if (!this.checkClickDelay())
            return;

        if (this.waiting == null) {
            this.waiting = node;
        }
        else {
            int weight;
            while (true) {
                try {
                    weight = Integer.parseInt(Greenfoot.ask("Gewicht der Kante?"));
                    break;
                }
                catch (Exception e) {}
            }
            this.graph.addEdge(node, this.waiting, weight);
            this.graph.addEdge(this.waiting, node, weight);
            this.waiting = null;
        }
        this.highlight(this.waiting, MyWorld.SELECT_COLOR);
    }    

    public void find(Node ball) {
        // TODO
    }

    public void act() {
        this.clear();
        this.addNode();
        this.draw();
    }

    public void clear() {
        this.removeObjects(this.graph.getNodes());
    }

    public void draw() {
        for (Node node : this.graph.getNodes())
            node.addTo(this);

        GreenfootImage bg = this.getBackground();
        for (Node from : this.graph.getNodes()) {
            for (Node to : this.graph.getNodes()) {
                int weight = graph.getEdge(from, to);
                if (from == to || weight == -1)
                    continue;

                // draw edge
                int[] fromPos = from.getPos();
                int[] toPos = to.getPos();
                bg.setColor(Color.BLACK);
                bg.drawLine(fromPos[0], fromPos[1], toPos[0], toPos[1]);

                // calculate midpoint
                int[] midpoint = new int[2];
                for (int i = 0; i != midpoint.length; ++i)
                    midpoint[i] = (fromPos[i] + toPos[i]) / 2;

                // draw weight
                bg.setColor(Color.BLACK);
                bg.fillOval(midpoint[0] - this.WEIGHT_RADIUS,
                    midpoint[1] - this.WEIGHT_RADIUS,
                    2 * this.WEIGHT_RADIUS,
                    2 * this.WEIGHT_RADIUS);

                bg.setColor(Color.WHITE);
                this.showText(Integer.toString(weight), midpoint[0], midpoint[1]);
            }
        }
        this.setBackground(bg);
    }

    public void addNode() {
        // not enough time passed (to filter bounce)
        if (!this.checkClickDelay())
            return;
        MouseInfo m = Greenfoot.getMouseInfo();

        // no mouse in current viewport
        if (m == null)
            return;

        // actor clicked
        if (m.getActor() != null)
            return;

        // no left click
        if (m.getButton() != 1)
            return;

        Node node = new Node(Greenfoot.ask("Name des Kontens?"),
                MyWorld.NODE_COLOR,
                m.getX(),
                m.getY());

        try {
            this.graph.addNode(node);
        }
        catch(IllegalArgumentException e) {
        }
    }

    private void highlight(Node ball, Color c) {
        // remove all other highlights
        for (Node other : this.graph.getNodes())
            other.setColor(MyWorld.NODE_COLOR);

        if (ball == null)
            return;

        ball.setColor(c);
    }

    private boolean checkClickDelay() {
        boolean ret = (System.currentTimeMillis() - this.prevClickTime) < 500;
        this.prevClickTime = System.currentTimeMillis();
        return ret;
    }
}
