import java.util.*;


public class Graph<T> {
    private List<T> nodes;
    private List<List<Integer>> matrix;
    
    public Graph() {
        this.nodes = new ArrayList<>();
        this.matrix = new ArrayList<>();
    }
    
    public List<T> getNodes() {
        return this.nodes;
    }
    
    private T nodeByIndex(int index) {
        if (0 <= index && index < this.nodes.size())
            return this.nodes.get(index);
        return null;
    }
    
    private int indexByNode(T node) {
        for (int i = 0; i != this.nodes.size(); ++i)
            if (this.nodes.get(i).equals(node))
                return i;
        return -1;
    }
    
    public void addNode(T node) {
        if (this.indexByNode(node) != -1)
            throw new IllegalArgumentException("Node with name \'" + node.toString() + "\' exists already");
            
        // add node to nodes list
        this.nodes.add(node);
        
        // add new column to matrix
        for (List<Integer> row : this.matrix)
            row.add(-1);
            
        // add new row to matrix
        List<Integer> newRow = new ArrayList<>();
        for (int i = 0; i != this.matrix.size() + 1; ++i)
            newRow.add(-1);
        this.matrix.add(newRow);
    }
    
    public void addEdge(T from, T to, int weight) {
        if (this.indexByNode(from) == -1)
            throw new IllegalArgumentException("Node with name \'" + from.toString() + "\' doesn\'t exist");
        if (this.indexByNode(to) == -1)
            throw new IllegalArgumentException("Node with name \'" + to.toString() + "\' doesn\'t exist");
        if (weight <= 0)
            throw new IllegalArgumentException("Weight has to be a positive integer");
            
        int f = this.indexByNode(from);
        int t = this.indexByNode(to);
        this.matrix.get(f).set(t, weight);
    }
    
    public List<T> getAdjacent(T node) {
        int index = this.indexByNode(node);
        List<T> adjacent = new ArrayList<>();
        List<Integer> row = this.matrix.get(index);
        for (int i = 0; i != row.size(); ++i)
            if (row.get(i) >= 0)
                adjacent.add(this.nodes.get(i));
        return adjacent;
    }
    
    public int getEdge(T from, T to) {
        int f = this.indexByNode(from);
        int t = this.indexByNode(to);
        return this.getEdge(f, t);
    }
    
    public int getEdge(int from, int to) {
        return this.matrix.get(from).get(to);
    }
        
}
