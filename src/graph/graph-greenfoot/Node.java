import greenfoot.*;


public class Node extends Clickable {
    private Color color;
    private final String name;
    private int x, y;
    
    public Node(String name, Color color, int x, int y) {
        this.name = name;
        this.color = color;
        this.x = x;
        this.y = y;
        this.setImage();
    }
    
    public void addTo(World w) {
        w.addObject(this, this.x, this.y);
    }
    
    public int[] getPos() {
        return new int[]{this.x, this.y};
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setColor(Color color) {
        this.color = color;
        this.setImage();
    }
    
    public void setImage() {
        GreenfootImage text = new GreenfootImage(this.name,
                                                 MyWorld.NODE_TEXT_SIZE,
                                                 this.color,
                                                 Color.WHITE);
        GreenfootImage image = new GreenfootImage(text.getWidth() + 4 * MyWorld.NODE_BORDER,
                                                  text.getHeight() + 4 * MyWorld.NODE_BORDER);
        image.setColor(this.color);
        image.fill();
        image.setColor(Color.WHITE);
        image.fillRect(MyWorld.NODE_BORDER,
                       MyWorld.NODE_BORDER,
                       text.getWidth() + 2 * MyWorld.NODE_BORDER,
                       text.getHeight() + 2 * MyWorld.NODE_BORDER);
        
        image.drawImage(text, MyWorld.NODE_BORDER * 2, MyWorld.NODE_BORDER * 2);
        super.setImage(image);
    }
    
    public void onClick(MyWorld world) {
        world.addToEdgeQueue(this);
    }
    
    public String toString() {
        return "(" + this.name + ")";
    }
    
    public boolean equals(Object other) {
        if (other instanceof Node)
            return (((Node)other).getName().equals(this.name));
        return false;
    }
}
