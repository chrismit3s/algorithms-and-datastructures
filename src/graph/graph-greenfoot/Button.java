import greenfoot.*;


public abstract class Button extends Clickable {
    protected final String text;
    protected final Color color;
    
    public Button(final String text, final Color color) {
        this.text = text;
        this.color = color;
        
        this.setImage();
    }
    
    public void setImage() {
        GreenfootImage image = new GreenfootImage(MyWorld.BUTTON_X, MyWorld.BUTTON_Y);
        
        image.setColor(this.color);
        image.fill();
        
        MyWorld.writeCentered(image, this.text, MyWorld.BUTTON_Y * 4 / 5, Color.WHITE);
        this.setImage(image);
    }
}
