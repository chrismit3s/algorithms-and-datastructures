import java.lang.IndexOutOfBoundsException;


class LinkedList<T> {
    private Node<T> head;
    
    /* Constructor
     */
    public LinkedList() {
        this.head = new Node<T>(null);
    }
    
    /* Getter and setter
     */
    public Node<T> getHead() {
        return this.head;
    }
    
    public void setHead(Node<T> head) {
        this.head = head;
    }
    
    /* Methods
     */
    public int wrapIndex(int x, int step) {
        while (x < 0)
            x += step;
        return x;
    }

	public boolean checkIndex(int index, int len) {
		return (-len <= index && index < len);
	}
    
    public int length() {
        return this.head.numNodesAfter();
    }
    
    public boolean isEmpty() {
        return this.head.getNext() == null;
    }
    
    public T getValue(int index) {
		/* gets the value of the node at position index
		 */
        // check index
		int len = this.length();
		if (!this.checkIndex(index, len))
			throw new IndexOutOfBoundsException("Cannot get index " + index + " in list of length " + this.length());
		index = this.wrapIndex(index, len);
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
            
		// get value
        return n.getNext().getValue();
    }
    
    public void setValue(int index, T d) {
		/* sets the value of the node at position index
		 */
        // check index
		int len = this.length();
		if (!this.checkIndex(index, len))
			throw new IndexOutOfBoundsException("Cannot set index " + index + " in list of length " + this.length());
		index = this.wrapIndex(index, len);
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
            
		// set value
        n.getNext().setValue(d);
    }

	public void pushNode(T d) {
		this.insertNode(-1, d);
	}

	public T popNode() {
		return this.removeNode(-1);
	}
    
    public void insertNode(int index, T d) {
		/* inserts a node with value d at position index
		 */
        // check index
		int len = this.length() + 1;  // +1 for all spots (before each member and at the very end)
		if (!this.checkIndex(index, len))
			throw new IndexOutOfBoundsException("Cannot insert at index " + index + " in list of length " + this.length() + " + 1");
		index = this.wrapIndex(index, len);
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
        
        // insert node
        n.insertAfter(new Node<T>(d));
    }
    
    public T removeNode(int index) {
		/* removes a node at position index and returns
		 * its value
		 */
        // check index
		int len = this.length();
		if (!this.checkIndex(index, len))
			throw new IndexOutOfBoundsException("Cannot remove index " + index + " in list of length " + this.length());
		index = this.wrapIndex(index, len);
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
        
        // remove node
        return n.removeAfter().getValue();
    }

	public String toString() {
		Node next = this.head.getNext();
		if (next == null)
			return "";
		else
			return next.toStringFromHere();
	}
}
