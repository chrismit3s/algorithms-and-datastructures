
class Node<T> {
	/* Attributes
	 */
	private T value;
	private Node<T> next;

	/* Constructors
	 */
	public Node(T value) {
		this.value = value;
		this.next = null;
	}

	public Node(T value, Node<T> next) {
		this.value = value;
		this.next = next;
	}

	/* Getters and setters
	 */
	public void setNext(Node<T> next) {
		this.next = next;
	}

	public Node<T> getNext() {
		return this.next;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public T getValue() {
		return this.value;
	}

	/* Methods
	 */
    public int numNodesAfter() {
        return (this.next == null) ? 0 : (this.next.numNodesAfter() + 1);
    }
    
    public void insertAfter(Node<T> n) {
        n.setNext(this.getNext());
        this.setNext(n);
    }
    
    public Node<T> removeAfter() {
        if (this.next == null)
            throw new NullPointerException("No next node in list to be removed");
            
        Node<T> n = this.next;
        this.setNext(n.getNext());
        n.setNext(null);
        
        return n;
    }

	public String toStringFromHere() {
		String s = "";
		s += this.value;
		s += " ";
		s += (this.next != null) ? this.next.toStringFromHere() : "";
		return s;
	}
}