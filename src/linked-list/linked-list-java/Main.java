
class Main {
	public static void main(String[] args) {
		LinkedList<Integer> list = new LinkedList<Integer>();

		for (int i = 0; i != 20; ++i)
			list.pushNode(i * i);
		System.out.println(list);

		list.popNode();
		System.out.println(list);

		list.removeNode(3);
		System.out.println(list);

		list.removeNode(0);
		System.out.println(list);

		list.removeNode(-2);
		System.out.println(list);

		list.insertNode(3, 1337);
		System.out.println(list);

		list.insertNode(0, 9999);
		System.out.println(list);

		list.insertNode(-2, 1234);
		System.out.println(list);
	}
}