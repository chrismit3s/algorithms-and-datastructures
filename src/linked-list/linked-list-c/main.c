#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef enum err_t {
	ALL_OK = 0,
	LIST_TOO_SHORT = -1
} err_t;

typedef struct node_t {
	int value;
	struct node_t *next;
} node_t;


void err_check(err_t e);
const char *err_to_str(err_t e);
int wrap_around(int x, int step);

void print_list(node_t *head);
void free_list(node_t *head);

int length_of_list(node_t *head);

err_t get_node(node_t *head, int index, int *value);
err_t set_node(node_t *head, int index, int value);

err_t insert_node(node_t *head, int index, int value);
err_t push_node(node_t *head, int value);
err_t append_node(node_t *head, int value);

err_t remove_node(node_t *head, int index, int *value);
err_t pop_node(node_t *head, int *value);


int main(void) {
	node_t head = { .value = 0, .next = NULL };
	err_t e;
	int x;

	printf("List: ");
	print_list(&head);
	printf("Len: %i\n\n", length_of_list(&head));

	for (int i = 0; i != 10; ++i) {
		err_check(push_node(&head, i * i));
	}


	printf("List: ");
	print_list(&head);
	printf("Len: %i\n\n", length_of_list(&head));

	err_check(insert_node(&head, 5, 1337));

	printf("List: ");
	print_list(&head);
	printf("Len: %i\n\n", length_of_list(&head));

	err_check(remove_node(&head, 3, &x));
	printf("Removed: %i (at index 3)\n", x);

	printf("List: ");
	print_list(&head);
	printf("Len: %i\n\n", length_of_list(&head));

	for (int i = 0; i != 4; ++i) {
		err_check(pop_node(&head, &x));
		printf("Popped: %i\n", x);
	}

	printf("List: ");
	print_list(&head);
	printf("Len: %i\n\n", length_of_list(&head));

	free_list(&head);
	return EXIT_SUCCESS;
}


void err_check(err_t e) {
	if (e != ALL_OK) {
		fprintf(stderr, err_to_str(e));
		exit(EXIT_FAILURE);
	}
}

const char *err_to_str(err_t e) {
	switch (e) {
	case ALL_OK:
		return "all ok";
		break;
	case LIST_TOO_SHORT:
		return "list too short";
		break;
	}
}

int wrap_around(int x, int step) {
	while (x < 0)
		x += step;
	return x % step;
}

void print_list(node_t *head) {
	if (head->next == NULL)
		printf("NULL");
	for (node_t *p = head->next;  // skip head element
	     p != NULL;
	     p = p->next)
		printf("%i ", p->value);
	printf("\n");
}

void free_list(node_t *head) {
	node_t *prev = NULL;
	for (node_t *p = head->next;  // skip head element
	     p != NULL;
	     p = p->next) {
		free(prev);
		prev = p;
	}
}

/* returns the number of elements after head until NULL is
 * found
 */
int length_of_list(node_t *head) {
	return (head->next == NULL) ? 0 : (length_of_list(head->next) + 1);
}

err_t get_node(node_t *head, int index, int *value) {
	// sanitize index for pythonic wrap-around
	index = wrap_around(index, length_of_list(head));

	// goto index
	int i;
	node_t *p;
	for (i = 0, p = head->next;  // skip head element
	     i != index && p != NULL;
	     p = p->next, ++i);

	// reached list end before index
	if (i != index && p == NULL)
		return LIST_TOO_SHORT;

	// get node value
	*value = p->value;

	return ALL_OK;
}

err_t set_node(node_t *head, int index, int value) {
	// sanitize index for pythonic wrap-around
	index = wrap_around(index, length_of_list(head));

	// goto index
	int i;
	node_t *p;
	for (i = 0, p = head->next;  // skip head element
	     i != index && p != NULL;
	     p = p->next, ++i);

	// reached list end before index
	if (i != index && p == NULL)
		return LIST_TOO_SHORT;

	// get node value
	p->value = value;

	return ALL_OK;
}

/* inserts a node containing value before index (so afterwards,
 * its indexed by [index])
 */
err_t insert_node(node_t *head, int index, int value) {
	// sanitize index for pythonic wrap-around
	index = wrap_around(index, length_of_list(head) + 1);  // +1 for all spots (before each member and at the very end)

	// goto index
	int i;
	node_t *p;
	for (i = 0, p = head;
	     i != index && p->next != NULL;
	     p = p->next, ++i);

	// reached list end before index
	if (i != index && p->next == NULL)
		return LIST_TOO_SHORT;

	// alloc and set node
	node_t *node = malloc(sizeof(node_t));
	node->value = value;
	node->next = p->next;
	p->next = node;

	return ALL_OK;
}

/* inserts a node at the end of the the list
 */
err_t push_node(node_t *head, int value) {
	return insert_node(head, -1, value);
}

/* inserts a node at the start of the the list
 */
err_t append_node(node_t *head, int value) {
	return insert_node(head, 0, value);
}

/* removes the node indexed by [index] and returns its value
 */
err_t remove_node(node_t *head, int index, int *value) {
	// sanitize index for pythonic wrap-around
	index = wrap_around(index, length_of_list(head));

	// goto index
	int i;
	node_t *p;
	for (i = 0, p = head;
	     i != index && p->next != NULL;
	     p = p->next, ++i);

	// reached list end before index
	if (i != index && p->next == NULL)
		return LIST_TOO_SHORT;

	// get and free node
	node_t *node = p->next;
	*value = node->value;
	p->next = node->next;
	free(node);

	return ALL_OK;
}

err_t pop_node(node_t *head, int *value) {
	return remove_node(head, -1, value);
}
