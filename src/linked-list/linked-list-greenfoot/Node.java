
public class Node<T> {
    private T value;
    private Node<T> next;
    
    /* Constructor
     */
    public Node(T value) {
        this.value = value;
    }
    
    /* Getter and setter
     */
    public T getValue() {
        return this.value;
    }
    
    public void setValue(T value) {
        this.value = value;
    }
    
    public Node<T> getNext() {
        return this.next;
    }
    
    public void setNext(Node<T> next) {
        this.next = next;
    }
    
    /* Methods
     */
	public void sortFromHere() {
		if (this.next == null)
			return;

		// get min
		Node<T> minNode = this.minFromHere();
		
		// swap
		T tmp = minNode.getValue();
		minNode.setValue(this.getValue());
		this.setValue(tmp);
		
		// sort the rest
		this.next.sortFromHere();
	}

    public Node<T> minFromHere() {
		if (this.next == null)
			return this;

		Node<T> minNode = this.next.minFromHere();
		return ((int)minNode.getValue() < (int)this.next.getValue()) ? minNode : this.next;  // TODO
    }
    
    public int findFromHere(T needle) {
		// check for match
        if (this.value == needle)
            return 0;

		// check next node
        if (this.next == null)
            return -1;

		int dist = this.next.findFromHere(needle);
		return (dist == -1) ? -1 : (dist + 1);
    }

	public void insertSorted(Node<T> n) {
		if (this.next == null || (int)this.next.getValue() >= (int)n.getValue())
			this.insertAfter(n);
		else
			this.next.insertSorted(n);
	}
    
    public void insertAfter(Node<T> n) {
        n.setNext(this.getNext());
        this.setNext(n);
    }
    
    public Node<T> removeAfter() {
        if (this.next == null)
            return null;  // ERROR
            
        Node n = this.next;
        this.setNext(n.getNext());
        n.setNext(null);
        
        return n;
    }
    
    public int numNodesAfter() {
        return (this.next == null) ? 0 : (this.next.numNodesAfter() + 1);
    }
    
    public String toString() {
        return "Node: " + ((this.value == null) ? "NULL" : this.value.toString());
    }
}
