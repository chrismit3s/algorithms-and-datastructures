public class LinkedList<T> {
    private Node<T> head;
    
    /* Constructor
     */
    public LinkedList() {
        this.head = new Node<T>(null);
    }
    
    /* Getter and setter
     */
    public Node<T> getHead() {
        return this.head;
    }
    
    public void setHead(Node<T> head) {
        this.head = head;
    }
    
    /* Methods
     */
    public int length() {
        return this.head.numNodesAfter();
    }
    
    public boolean isEmpty() {
        return this.head.getNext() == null;
    }

	public void sort() {
		this.head.getNext().sortFromHere();
	}
    
    public void swap(int i, int j) {
        T x = this.getValue(i);
        this.setValue(i, this.getValue(j));
        this.setValue(j, x);
    }
    
    public int find(T needle) {
        return this.head.getNext().findFromHere(needle);
    }
    
    public int wrapIndex(int x, int step) {
        while (x < 0)
            x += step;
        return x;
    }
    
    public T getValue(int index) {
        // check if there are any elements in the list
        if (this.isEmpty())
            return null;  // ERROR
        
        // sanitize index for pythonic wrap around
        index = this.wrapIndex(index, this.length());
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head.getNext();
             i != index && n != null;
             ++i, n = n.getNext());
        
        // check for list end before index
        if (i != index && n == null)
            return null;  // ERROR
            
        return n.getValue();
    }
    
    public int setValue(int index, T v) {
        // check if there are any elements in the list
        if (this.isEmpty())
            return -1;  // ERROR
        
        // sanitize index for pythonic wrap around
        index = this.wrapIndex(index, this.length());
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head.getNext();
             i != index && n != null;
             ++i, n = n.getNext());
        
        // check for list end before index
        if (i != index && n == null)
            return -1;  // ERROR
            
        n.setValue(v);
        return 0;
    }
    
    public int insertNode(int index, T v) {
        // sanitize index for pythonic wrap around
        index = this.wrapIndex(index, this.length() + 1);  // +1 for all spots (before each member and at the very end)
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
        
        // check for list end before index
        if (i != index && n.getNext() == null)
            return -1;  // ERROR
        
        // insert node
        n.insertAfter(new Node(v));
        return 0;
    }
    
    public T removeNode(int index) {
        // check if there are any elements in the list
        if (this.isEmpty())
            return null;  // ERROR
        
        // sanitize index for pythonic wrap around
        index = this.wrapIndex(index, this.length());
        
        // goto index
        int i;
        Node<T> n;
        for (i = 0, n = this.head;
             i != index && n.getNext() != null;
             ++i, n = n.getNext());
        
        // check for list end before index
        if (i != index && n.getNext() == null)
            return null;  // ERROR
        
        // remove node
        return n.removeAfter().getValue();
    }
    
    public String toString() {
        String s = "";
        for (Node<T> n = this.head.getNext(); n != null; n = n.getNext())
            s += n.getValue().toString() + " ";
        return s;
    }
}
