import greenfoot.*;

public class ToggleSwitch extends MultiStateSwitch {
    /* Constructors
     */
    public ToggleSwitch(int sizeX, int sizeY, Color[] colors, String[] texts) {
		super(sizeX, sizeY, colors, texts, 2);
    }
    
    /* Methods
     */
    public void updateImage() {
        GreenfootImage image = new GreenfootImage(this.sizeX, this.sizeY);
        
        // draw to rects
        Color c1 = this.colors[this.state], c2 = Color.GRAY;
        image.setColor((this.state != 0) ? c2 : c1);
        image.fillRect(0, 0, this.sizeX / 2, this.sizeY);
        image.setColor((this.state != 0) ? c1 : c2);
        image.fillRect(this.sizeX / 2, 0, this.sizeX / 2, this.sizeY);
        
        // draw text
        image.setColor(Color.WHITE);
        image.setFont(new Font("futura md bt", true, false, sizeY / 2));
        image.drawString(this.texts[this.state], this.sizeX / 15 + this.state * this.sizeX / 2, this.sizeY * 2 / 3);
        this.setImage(image);
    }
}
