import greenfoot.*;


public class SortList extends Button {
    /* Constructors
     */
    public SortList(int sizeX, int sizeY) {
        super(sizeX, sizeY, new Color(251, 187, 5), "SORT");
    }
    
    /* Methods
     */
    public void onClick(World w) {
        ((MyWorld)w).sortList();
    }
}
