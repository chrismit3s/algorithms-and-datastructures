import greenfoot.*;


public class IntBall extends Actor {
    private int value;
    private int sizeX;
    private int sizeY;
    
    /* Constructors
     */
    public IntBall(int value, int sizeX, int sizeY) {
        this.value = value;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        
        GreenfootImage image = new GreenfootImage(sizeX, sizeY);
        image.setColor(Color.BLUE);
        image.fillOval(0, 0, sizeX, sizeY);
        image.setColor(Color.WHITE);
        image.setFont(new Font("futura md bt", true, false, sizeY / 2));
        image.drawString(Integer.toString(this.value), this.getOffset(), this.sizeY * 2 / 3);
        this.setImage(image);
    }
    
    /* Getter and setter
     */
    public int getValue() {
        return this.value;
    }
    
    public void setValue(int value) {
        this.value = value;
    }
    
    /* Methods
     */
    private int getOffset() {
        int d = this.digits();
        switch (d) {
            case 0:
            case 1:
            case 2:
                return this.sizeX / (2 * d + 1);
            case 3:
            default:
                return 0;
        }
    }
    
    public int digits() {
        int i, div;
        for (i = 0, div = 1;
             this.value / div != 0;
             ++i, div *= 10);
        return i;
    }
    
    public String toString() {
        return "(" + Integer.toString(this.value) + ")";
    }
    
	public int toInteger() {
		return this.value;
	}
}
