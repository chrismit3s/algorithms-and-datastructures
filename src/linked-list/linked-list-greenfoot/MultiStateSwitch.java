import greenfoot.*;

public class MultiStateSwitch extends Actor {
    protected int sizeX, sizeY;
    protected Color[] colors;
    protected String[] texts;
    protected int state, numStates;
    
    /* Constructors
     */
    public MultiStateSwitch(int sizeX, int sizeY, Color[] colors, String[] texts, int numStates) {
        // set attributes
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.colors = colors;
        this.texts = texts;
        this.state = 0;
        this.numStates = numStates;
        
        this.updateImage();
    }
    
    /* Getters and setters
     */
    public int getState() {
        return this.state;
    }
    
    public void setState(int state) {
        this.state = state % this.numStates;
    }
    
    /* Methods
     */
    public void act() {
        if (Greenfoot.mouseClicked(this))
            this.next();
    }
    
    public void updateImage() {
        GreenfootImage image = new GreenfootImage(this.sizeX, this.sizeY);
        
        // draw rect
        image.setColor(this.colors[this.state]);
        image.fillRect(0, 0, this.sizeX, this.sizeY);
        
        // draw text
        image.setColor(Color.WHITE);
        image.setFont(new Font("futura md bt", true, false, sizeY / 2));
        image.drawString(this.texts[this.state], this.sizeX / 15, this.sizeY * 2 / 3);
        this.setImage(image);
    }
    
    public void next() {
        this.setState(this.state + 1);
        this.updateImage();
    }
}
