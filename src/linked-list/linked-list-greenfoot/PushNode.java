import greenfoot.*;


public class PushNode extends Button {
    /* Constructors
     */
    public PushNode(int sizeX, int sizeY) {
        super(sizeX, sizeY, new Color(24, 138, 7), "PUSH");
    }
    
    /* Methods
     */
    public void onClick(World w) {
        ((MyWorld)w).pushNode();
    }
}
