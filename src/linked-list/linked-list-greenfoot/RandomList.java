import greenfoot.*;


public class RandomList extends Button {
    /* Constructors
     */
    public RandomList(int sizeX, int sizeY) {
        super(sizeX, sizeY, new Color(47, 137, 97), "RANDOM");
    }
    
    /* Methods
     */
    public void onClick(World w) {
        ((MyWorld)w).randomList();
    }
}
