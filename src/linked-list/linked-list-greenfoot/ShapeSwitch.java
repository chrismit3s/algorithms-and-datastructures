import greenfoot.*;


public class ShapeSwitch extends ToggleSwitch {
    /* Constructors
     */
    public ShapeSwitch(int sizeX, int sizeY) {
        super(sizeX, sizeY,
              new Color[]{Color.BLUE, Color.BLUE},
              new String[]{"SQUARE", "BALL"});
    }
}
