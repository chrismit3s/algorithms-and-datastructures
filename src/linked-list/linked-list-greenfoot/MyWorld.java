import greenfoot.*;
import java.util.Random;


public class MyWorld extends World {
    private LinkedList<Actor> list;
    public static final int NUM_BUTTONS = 6;
    //public static final int WORLD_X = 1280 * 3 / 5, WORLD_Y = 720 * 3 / 5;
    public static final int WORLD_X = 1280, WORLD_Y = 720;
    public static final int BUTTON_X = 2 * WORLD_X / (NUM_BUTTONS * 3 + 1), BUTTON_Y = BUTTON_X / 5;
    public static final int NODE_X = BUTTON_Y, NODE_Y = BUTTON_Y;
    private Actor[] buttons;
    
    /* Constructors
     */
    public MyWorld() {
        super(WORLD_X, WORLD_Y, 1);
        this.list = new LinkedList<Actor>();
        
        this.buttons = new Actor[this.NUM_BUTTONS];
        for (int i = 0; i != this.NUM_BUTTONS; ++i) {
            switch(i) {
                case 0:
                    this.buttons[i] = new PushNode(this.BUTTON_X, this.BUTTON_Y);
                    break;
                case 1:
                    this.buttons[i] = new PopNode(this.BUTTON_X, this.BUTTON_Y);
                    break;
                case 2:
                    this.buttons[i] = new SortList(this.BUTTON_X, this.BUTTON_Y);
                    break;
                case 3:
                    this.buttons[i] = new RandomList(this.BUTTON_X, this.BUTTON_Y);
                    break;
                case 4:
                    this.buttons[i] = new ModeSwitch(this.BUTTON_X, this.BUTTON_Y);
                    break;
                case 5:
                    this.buttons[i] = new ShapeSwitch(this.BUTTON_X, this.BUTTON_Y);
                    break;
            }
            this.addObject(this.buttons[i],
                           this.BUTTON_X * (3 * i + 2) / 2 ,
                           this.BUTTON_Y * 3 / 2);
        }
        this.draw();
    }
    
    /* Getter and setter
     */
    public LinkedList<Actor> getList() {
        return this.list;
    }
    
    public int getClickMode() {
        return ((MultiStateSwitch)this.buttons[this.NUM_BUTTONS - 2]).getState();
    }
    
    public int getShapeMode() {
        return ((MultiStateSwitch)this.buttons[this.NUM_BUTTONS - 1]).getState();
    }
    
    /* Methods
     */
    private void draw() {
        // initialize coords/background
        int x = (this.NODE_X + this.BUTTON_X) / 2;  // left edge of button aligned with the first node
        int y = 4 * this.NODE_Y;
        GreenfootImage bg = this.getBackground();
        bg.setColor(Color.BLACK);
        bg.setFont(new Font("futura md bt", true, false, this.NODE_Y / 2));
        bg.drawString("Length: " + Integer.toString(this.list.length()), this.NODE_X / 2, this.WORLD_Y - this.NODE_X / 2);
        
        // draw nodes + connections
        for (Node<Actor> n = this.list.getHead().getNext();
             n != null;
             n = n.getNext()) {
                 // draw node
                 this.addObject(n.getValue(), x, y);
                 
                 // draw connection if its not the last
                 if (n.getNext() != null)
                    bg.drawLine(x, y, x + 2 * this.NODE_X, y);
                 
                 // increment x coord
                 x += 2 * this.NODE_X;
        }
    }
    
    private void clear() {
        // clear background
        GreenfootImage bg = this.getBackground();
        bg.setColor(Color.WHITE);
        bg.fill();
        
        // remove nodes
        for (Node<Actor> n = this.list.getHead().getNext();
             n != null;
             n = n.getNext()) {
                 this.removeObject(n.getValue());
        }
    }
    
    public void pushNode() {
        this.clear();
        this.list.insertNode(-1, this.createNodeActor());
        this.draw();
    }
    
    public Actor popNode() {
        this.clear();
        Actor a = this.list.removeNode(-1);
        this.draw();
        return a;
    }
    
    public void sortList() {
        this.clear();
        this.list.sort();
        this.draw();
    }

    public void randomList() {
        this.clear();

		// clear list
		while (!this.list.isEmpty())
			this.list.removeNode(-1);

		// ask for len
		int len = this.askForInt("Number of random elements?");

		// fill list with random values
		int value;
		Actor a;
		Random rng = new Random();
		for (int i = 0; i != len; ++i) {
			value = rng.nextInt(1337 * 2 + 1) - 1338;
			a = (rng.nextInt(2) != 0) ? new IntBall(value, this.NODE_X, this.NODE_Y) : new IntSquare(value, this.NODE_X, this.NODE_Y);
			list.insertNode(0, a);
		}

        this.draw();
    }

	public int askForInt(String prompt) {
		int x;
        while (true) {
            try {
                x = Integer.parseInt(Greenfoot.ask(prompt));
                break;
            }
            catch (Exception e) {}
        }
		return x;
	}
    
    public Actor createNodeActor() {
        int x = this.askForInt("Value of node");
        Actor a = (this.getShapeMode() != 0)
                ? (new IntBall(x, this.NODE_X, this.NODE_Y))
                : (new IntSquare(x, this.NODE_X, this.NODE_Y));
        return a;
    }
    
    public void act() {
        for (Node<Actor> n = this.list.getHead(); n.getNext() != null; n = n.getNext()) {
            if (Greenfoot.mouseClicked(n.getNext().getValue())) {
                this.clear();
                
                switch (this.getClickMode()) {
					case 0: // INSERT SORTED
                        n.insertAfter(new Node(this.createNodeActor()));
						break;
					case 1: // INSERT BEFORE (n.next is the node that was clicked on)
                        n.insertAfter(new Node(this.createNodeActor()));
						break;
					case 2: // INSERT AFTER (n.next is the node that was clicked on)
                        n.getNext().insertAfter(new Node(this.createNodeActor()));
						break;
					case 3: // DELETE
                        n.removeAfter();
						break;
				}
                
                this.draw();
                break;  // as only one node can be clicked on at one point in time
            }
        }
    }
}
