import greenfoot.*;


public class ModeSwitch extends MultiStateSwitch {
    /* Constructors
     */
    public ModeSwitch(int sizeX, int sizeY) {
        super(sizeX, sizeY,
              new Color[]{new Color(24, 138, 7), new Color(24, 138, 7), new Color(24, 138, 7), new Color(227, 37, 37)},
              new String[]{"INSERT SORTED", "INSERT BEFORE", "INSERT AFTER", "DELETE"}, 4);
    }
}
