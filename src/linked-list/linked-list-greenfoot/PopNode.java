import greenfoot.*;


public class PopNode extends Button {
    /* Constructors
     */
    public PopNode(int sizeX, int sizeY) {
        super(sizeX, sizeY, new Color(227, 37, 37), "POP");
    }
    
    /* Methods
     */
    public void onClick(World w) {
        ((MyWorld)w).popNode();
    }
}
