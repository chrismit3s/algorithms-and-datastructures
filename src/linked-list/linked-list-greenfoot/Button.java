import greenfoot.*;

abstract public class Button extends Actor {
    private int sizeX, sizeY;
    private Color background;
    private String text;
    
    /* Constructors
     */
    public Button(int sizeX, int sizeY, Color background, String text) {
        // set attributes
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.background = background;
        this.text = text;
        
        // create image
        GreenfootImage image = new GreenfootImage(this.sizeX, this.sizeY);
        image.setColor(this.background);
        image.fill();
        image.setColor(Color.WHITE);
        image.setFont(new Font("futura md bt", true, false, this.sizeY / 2));
        image.drawString(this.text, this.sizeX / 15, this.sizeY * 2 / 3);
        this.setImage(image);
    }
    
    /* Methods
     */
    public void act() {
        if (Greenfoot.mouseClicked(this))
            this.onClick(this.getWorld());
    }
    
    abstract public void onClick(World w);
}
