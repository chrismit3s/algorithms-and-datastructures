import greenfoot.*;


public class IntBall extends Ball {
    private int value;
    
    public IntBall(int value) {
        super(Integer.toString(value), MyWorld.BALL_COLOR);
        this.value = value;
    }
    
    public void setImage() {
        GreenfootImage image = new GreenfootImage(2 * MyWorld.BALL_RADIUS,
                                                  2 * MyWorld.BALL_RADIUS);
        
        image.setColor(this.color);
        image.fillOval(0, 0,
                       2 * MyWorld.BALL_RADIUS,
                       2 * MyWorld.BALL_RADIUS);
        
        MyWorld.writeCentered(image, this.text, MyWorld.BALL_RADIUS * 3 / 2, Color.WHITE);
        this.setImage(image);
    }
    
    public void onClick(MyWorld world) {
        world.delete(this);
    }
    
    public int compareTo(Ball other) {
        if (!(other instanceof IntBall))
            return -1;
        return this.value - ((IntBall)other).value;
    }
    
    public boolean equals(Object other) {
        if (!(other instanceof IntBall))
            return false;
        return (this.value == ((IntBall)other).value);
    }
}