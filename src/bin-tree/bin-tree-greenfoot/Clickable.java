import greenfoot.*;


public abstract class Clickable extends Actor{
    public void act() {
        if (Greenfoot.mouseClicked(this))
            this.onClick((MyWorld)this.getWorld());
    }
    
    public abstract void onClick(MyWorld w);
}
