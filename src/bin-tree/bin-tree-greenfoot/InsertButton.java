import greenfoot.*;


public class InsertButton extends Button {
    public InsertButton() {
        super("INSERT", Color.RED);
    }
    
    public void onClick(MyWorld world) {
        int value = 0;
        while (true) {
            try {
                value = Integer.parseInt(Greenfoot.ask("Value to insert?"));
                break;
            }
            catch (NumberFormatException ignored) {}
        }
        
        world.insert(new IntBall(value));
    }
}