import greenfoot.*;


public abstract class Ball extends Clickable implements Comparable<Ball> {
    protected Color color;
    protected final String text;
    
    public Ball(final String text, Color color) {
        this.color = color;
        this.text = text;
        this.setImage();
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setColor(Color c) {
        this.color = c;
        this.setImage();
    }
    
    public abstract void setImage();
    
    public String toString() {
        return this.text;
    }
}
