import greenfoot.*;


public class NullBall extends Ball {
    public NullBall() {
        super("", MyWorld.BALL_COLOR);
        this.setImage();
    }
    
    public void setImage() {
        GreenfootImage image = new GreenfootImage(2 * MyWorld.BALL_RADIUS,
                                                  2 * MyWorld.BALL_RADIUS);
                                                  
        image.setColor(Color.WHITE);
        image.fillOval(0, 0,
                       2 * MyWorld.BALL_RADIUS - 1,
                       2 * MyWorld.BALL_RADIUS - 1);
        
        image.setColor(this.color);
        image.drawOval(0, 0,
                       2 * MyWorld.BALL_RADIUS - 1,
                       2 * MyWorld.BALL_RADIUS - 1);
                       
        this.setImage(image);
    }
    
    public void onClick(MyWorld world) {
    }
    
    public int compareTo(Ball other) {
        return this.equals(other) ? 0 : -1;
    }
    
    public boolean equals(Object other) {
        return (other instanceof NullBall) && (other != null);
    }
}
