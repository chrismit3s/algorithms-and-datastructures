import greenfoot.*;


public class BalanceButton extends Button {
    public BalanceButton() {
        super("BALANCE", Color.ORANGE);
    }
    
    public void onClick(MyWorld world) {
        world.balance();
    }
}