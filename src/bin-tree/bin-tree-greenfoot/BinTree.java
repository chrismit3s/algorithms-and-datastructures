import java.util.LinkedList;
import java.util.List;


public class BinTree<T extends Comparable<? super T>> {
    public T value;
    public BinTree<T> parent, leftChild, rightChild;
    
    public BinTree(final T value) {
        this(value, null, null);
    }

    private BinTree(final T value, final BinTree<T> leftChild, final BinTree<T> rightChild) {
        this.value = value;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public void addLeftChild(T value) {
        this.leftChild = new BinTree<>(value);
        this.leftChild.parent = this;
    }

    public void addRightChild(T value) {
        this.rightChild = new BinTree<>(value);
        this.rightChild.parent = this;
    }

    public boolean isLeftChild() {
        if (this.parent == null)
            return false;
        return (this.parent.leftChild == this);
    }

    public boolean isRightChild() {
        if (this.parent == null)
            return false;
        return (this.parent.rightChild == this);
    }

    public void swap(BinTree<T> other) {
        T temp = this.value;
        this.value = other.value;
        other.value = temp;
    }

    public int getHeight() {
        if (this.leftChild == null && rightChild == null)
            return 0;
        else if (this.leftChild != null)
            return this.leftChild.getHeight() + 1;
        else if (this.rightChild != null)
            return this.rightChild.getHeight() + 1;
        else {
            int lHeight = this.leftChild.getHeight();
            int rHeight = this.rightChild.getHeight();
            return (lHeight < rHeight ? rHeight : lHeight) + 1;
        }
    }

    public BinTree<T> findNode(T value) {
        if (value.compareTo(this.value) > 0 && this.rightChild != null)  // bigger
            return this.rightChild.findNode(value);
        else if (value.compareTo(this.value) < 0 && this.leftChild != null)  // smaller
            return this.leftChild.findNode(value);
        else  // equal or no child
            return this;
    }

    public boolean contains(T value) {
        BinTree<T> node = this.findNode(value);
        return ((value != null) && (node.value != null) && (value.compareTo(node.value) == 0));
    }

    public boolean insert(T value) {
        BinTree<T> node = this.findNode(value);

        // already contains node
        if (value.compareTo(node.value) == 0)  // equal
            return false;

        // add it
        if (value.compareTo(node.value) > 0)  // bigger
            node.addRightChild(value);
        else  // smaller
            node.addLeftChild(value);
        return true;
    }

    public BinTree<T> getMinNode() {
        if (this.leftChild == null)
            return this;
        else
            return this.leftChild.getMinNode();
    }

    public BinTree<T> getMaxNode() {
        if (this.rightChild == null)
            return this;
        else
            return this.rightChild.getMaxNode();
    }

    public BinTree<T> deleteNode() {
        // inner
        if (this.leftChild != null && rightChild != null) {
            BinTree<T> node = this.rightChild.getMinNode();
            this.swap(node);
            node.deleteNode();
            return this;
        }
        // single child
        else if (this.rightChild != null) {
            if (this.isLeftChild()) {
                this.parent.leftChild = this.rightChild;
                this.rightChild.parent = this.parent;
            }
            else if (this.isRightChild()) {
                this.parent.rightChild = this.rightChild;
                this.rightChild.parent = this.parent;
            }
            return this.rightChild;
        }
        else if (this.leftChild != null) {
            if (this.isLeftChild()) {
                this.parent.leftChild = this.leftChild;
                this.leftChild.parent = this.parent;
            }
            else if (this.isRightChild()) {
                this.parent.rightChild = this.leftChild;
                this.leftChild.parent = this.parent;
            }
            return this.leftChild;
        }
        // leaf
        else {
            if (this.isLeftChild())
                this.parent.leftChild = null;
            else if (this.isRightChild())
                this.parent.rightChild = null;
            return null;
        }
    }

    public BinTree delete(T value) {
        // cant delete if it doesnt exist
        if (!this.contains(value))
            return this;

        // tree contains value => findNode will find it
        BinTree<T> node = this.findNode(value);
        if (node.isLeftChild())
            node.parent.leftChild = node.deleteNode();
        else if (node.isRightChild())
            node.parent.rightChild = node.deleteNode();
        else  // root
            return node.deleteNode();
        return this;
    }

    public List<T> traverse(int order) {
        List<T> list = new LinkedList<>();
        switch (order) {
            case -1:
                list.add(this.value);
                if (this.leftChild != null)
                    list.addAll(this.leftChild.traverse(order));
                if (this.rightChild != null)
                    list.addAll(this.rightChild.traverse(order));
                break;
            case 0:
                if (this.leftChild != null)
                    list.addAll(this.leftChild.traverse(order));
                list.add(this.value);
                if (this.rightChild != null)
                    list.addAll(this.rightChild.traverse(order));
                break;
            case 1:
                if (this.leftChild != null)
                    list.addAll(this.leftChild.traverse(order));
                if (this.rightChild != null)
                    list.addAll(this.rightChild.traverse(order));
                list.add(this.value);
                break;
            default:
                throw new IllegalArgumentException("Tree traversal order should be -1, 0 or 1");
        }
        return list;
    }
    
    public int countNodes() {
        int count = 1;
        if (this.rightChild != null)
            count += this.rightChild.countNodes();
        if (this.leftChild != null)
            count += this.leftChild.countNodes();
        return count;
    }

    public BinTree<T> nextNode() {
        // get root
        BinTree<T> root = this;
        while (root.parent != null)
            root = root.parent;

        // get sorted representation of tree
        List<T> list = root.traverse(0);

        // find this node value and take the next
        int index = list.indexOf(this.value) + 1;

        // check if there is a next node
        if (index >= list.size())
            return null;
        else
            return root.findNode(list.get(index));
    }
}