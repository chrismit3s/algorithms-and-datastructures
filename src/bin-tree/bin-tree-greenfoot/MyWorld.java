import greenfoot.*;
import java.util.*;
import java.lang.reflect.*;


public class MyWorld extends World {
    public static final int BUTTON_X = 100, BUTTON_Y = 30, BUTTON_SPACE = 10;
    public static final int BALL_RADIUS = 10;
    public static final int LEVEL_STEP = 40;
    public static final Color BALL_COLOR = Color.BLUE, HIGHLIGHT_COLOR = Color.MAGENTA;
    public static final Class<?>[] BUTTONS = {
        InsertButton.class,
        BalanceButton.class,
        FindButton.class,
        RandomButton.class
    };
    private BinTree<Ball> root;
    private List<NullBall> nulls;
    private Random rng;
    
    public static void writeCentered(GreenfootImage img, String text, int size, Color color, int x, int y) {
        GreenfootImage textImg = new GreenfootImage(text, size, color, null);  
        x -= textImg.getWidth() / 2;
        y -= textImg.getHeight() / 2;
        img.drawImage(textImg, x, y);
    }
    
    public static void writeCentered(GreenfootImage img, String text, int size, Color color) {
        writeCentered(img, text, size, color, img.getWidth() / 2, img.getHeight() / 2);
    }
    
    public static int min(int a, int b) {
        return (a < b) ? a : b;
    }
    
    public static int max(int a, int b) {
        return (a > b) ? a : b;
    }
    
    public MyWorld() throws InstantiationException,
                            IllegalAccessException,
                            IllegalArgumentException,
                            InvocationTargetException {
        super(600, 400, 1);
        
        this.rng = new Random();
        
        this.root = null;
        this.nulls = new LinkedList<>();
        
        for (int i = 0; i != MyWorld.BUTTONS.length; ++i)
            this.addObject((Button)MyWorld.BUTTONS[i].getDeclaredConstructors()[0].newInstance(),
                           MyWorld.BUTTON_SPACE + MyWorld.BUTTON_X / 2 + (MyWorld.BUTTON_SPACE + MyWorld.BUTTON_X) * i,
                           MyWorld.BUTTON_SPACE + MyWorld.BUTTON_Y / 2);
    }
    
    public void generateRandomTree() {
        this.generateRandomTree(this.rng.nextInt(20) + 20);
    }
    
    public void generateRandomTree(int size) {
        this.generateRandomTree(size, -this.rng.nextInt(10), this.rng.nextInt(20));
    }
    
    public void generateRandomTree(int size, int min, int max) {
        this.clearAll();
        this.root = new BinTree<Ball>(new IntBall(this.rng.nextInt(max - min) + min));
        while (this.root.countNodes() < MyWorld.min(size, max - min))
            this.root.insert(new IntBall(this.rng.nextInt(max - min) + min));
        this.drawTree();
    }
    
    public void insert(Ball ball) {
        this.clearAll();
        if (root == null)
            root = new BinTree(ball);
        else
            root.insert(ball);
        this.drawTree();
    }
    
    public void delete(Ball ball) {
        this.clearAll();
        this.root = this.root.delete(ball);
        this.drawTree();
    }
    
    public void balance() {
        if (this.root == null)
            return;
            
        List<Ball> res = this.balanceRec(this.root.traverse(0));
        this.root = new BinTree<>(res.remove(0));
        for (Ball b : res)
            this.root.insert(b);
    }
    
    private List<Ball> balanceRec(List<Ball> l) {
        int m = l.size() / 2;
        List<Ball> res = new LinkedList<>();
        
        res.add(l.get(m));
        if (m != 0) {
            res.addAll(this.balanceRec(l.subList(0, l.size() / 2)));
            res.addAll(this.balanceRec(l.subList(l.size() / 2, l.size())));
        }
        return res;
    }
    
    public void find(Ball ball) {
        if (this.root == null || ball == null)
            return;
            
        Ball found = this.root.findNode(ball).value;
        
        if (ball.compareTo(found) == 0)
            this.highlight(found);
    }
    
    public void act() {
        this.clearAll();
        this.drawTree();
        // TODO draw node count (this.root.countNodes())
    }
    
    private void clearAll() {
        if (this.root != null)
            this.removeObjects(this.root.traverse(0));
        this.removeObjects(this.nulls);
        this.nulls.clear();
        GreenfootImage bg = this.getBackground();
        bg.setColor(Color.WHITE);
        bg.fill();
    }
    
    private void highlight(Ball ball) {
        if (this.root == null || ball == null)
            return;
            
        // remove all other highlights
        for (Ball other : this.root.traverse(0))
            other.setColor(MyWorld.BALL_COLOR);
            
        ball.setColor(MyWorld.HIGHLIGHT_COLOR);
    }
    
    private void drawTree() {
        this.drawSubTree(this.root,
                         MyWorld.LEVEL_STEP,
                         this.getWidth() - MyWorld.LEVEL_STEP,
                         0);
    }
    
    private void drawSubTree(BinTree root, int left, int right, int level) {
        int x = (left + right) / 2;
        int y = level * MyWorld.LEVEL_STEP + 3 * MyWorld.BUTTON_SPACE + MyWorld.BUTTON_Y;  // level + button_padd
        
        if (root == null) {
            this.nulls.add(0, new NullBall());
            this.addObject(this.nulls.get(0), x, y);
        }
        else {
            GreenfootImage bg = this.getBackground();
            bg.setColor(Color.BLACK);
            
            this.addObject((Ball)(root.value), x, y);
            
            bg.drawLine(x, y, (x + left) / 2, y + MyWorld.LEVEL_STEP);
            this.drawSubTree(root.leftChild, left, x, level + 1);
            
            bg.drawLine(x, y, (x + right) / 2, y + MyWorld.LEVEL_STEP);
            this.drawSubTree(root.rightChild, x, right, level + 1);
        }
    }
}
