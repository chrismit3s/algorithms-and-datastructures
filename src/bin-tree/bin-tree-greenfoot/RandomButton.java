import greenfoot.*;


public class RandomButton extends Button {
    public RandomButton() {
        super("RANDOM", Color.GREEN);
    }
    
    public void onClick(MyWorld world) {
        world.generateRandomTree();
    }
}