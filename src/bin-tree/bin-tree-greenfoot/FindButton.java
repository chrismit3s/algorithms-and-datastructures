import greenfoot.*;


public class FindButton extends Button {
    public FindButton() {
        super("FIND", MyWorld.HIGHLIGHT_COLOR);
    }
    
    public void onClick(MyWorld world) {
        int value = 0;
        while (true) {
            try {
                value = Integer.parseInt(Greenfoot.ask("Value to search for?"));
                break;
            }
            catch (NumberFormatException ignored) {}
        }
        
        world.find(new IntBall(value));
    }
}